class UserMailer < ApplicationMailer
  def welcome
    @user = "jackson"
    @site_name = "working_with_email.com.br"

    mail to: "to@example.org", subject: "Seja bem vindo a plataforma" 
  end

  def niver 
    attachments.inline['niver.jpg'] = File.read("#{Rails.root}/app/assets/images/niver.jpg")

    @user = "birthday person"
    @email_to = "birthday_person@example.com.br"
    mail to: @email_to, subject: "Feliz aniversário #{@user}, muitos anos de vida!"
  end
end
