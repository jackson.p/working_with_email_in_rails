# README

Este projeto tem o intuito de servir como diretório para estudo do disparo de emails com o rails

# cli
no rails ja existe uma classe chamada mailer, e podemos gerar subclasses dela usando o cli, de uma maneira similar aos controllers.
```
  $ rails g mailer Example
```
este comando irá gerar uma abstração para trabalharmos com emails de forma mais especifica, por exemplos emails relacionados as boas vindas a plataforma e recuperação de senha etc.

acrescentando um parâmetro a mais na frente do nome do mailer irá gerar também um método dentro dele. Cada método dentro de um mailer precisa ter seu layout correspondente dentro da pasta views.

um exemplo
```
$ rails g mailer User welcome
```
podemos usar o disparo de emails usando o console do rails dessa forma:
```
$ rails c
$ UserMailer.welcome.deliver.now!
```

o comando acima aciona o methodo welcome dentro da classe UserMailer e renderiza seu view correspondente welcome.html.erb

# variáveis 
Sabemos que todos emails precisam ter o nome do usuário geralmente algumas informações sobre sua conta, para isso podemos incluir variáveis aos nossos emails por meio das variáveis de classe.

Nós declaramos ela dentro da classe que herda do AplicationMailer e assim ela será visível dentro dos arquivos .erb correspondentes aos métodos da classe.

no exemplo do UserMailer criamos as variáveis @user e @site_name. Dentro do .erb basta user <%= @user %> e <%= @site_name %>

# anexar arquivos
podemos anexar arquivos através da função "attachments.inline"

